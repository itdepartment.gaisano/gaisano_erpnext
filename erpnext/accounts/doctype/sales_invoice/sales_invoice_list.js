// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt

// render
frappe.listview_settings['Sales Invoice'] = {
	add_fields: ["customer", "customer_name", "base_grand_total", "outstanding_amount", "due_date", "company",
		"currency", "is_return"],
	get_indicator: function(doc) {
		var status_color = {
			"Draft": "grey",
			"Unpaid": "orange",
			"Paid": "green",
			"Return": "darkgrey",
			"Credit Note Issued": "darkgrey",
			"Unpaid and Discounted": "orange",
			"Overdue and Discounted": "red",
			"Overdue": "red",
			"Partially Paid": "orange"

		};
		if (doc.paid_amount > doc.grand_total){
			return [__("Paid"), status_color["Paid"], "status,=,"+"Paid"];
		}
		if (doc.outstanding_amount != 0 && doc.outstanding_amount < doc.rounded_total){
				return [__("Partially Paid"), status_color["Partially Paid"], "status,=,"+"Partially Paid"];
		}
		else{
			return [__(doc.status), status_color[doc.status], "status,=,"+doc.status];
		}

	},
	right_column: "grand_total"
};
