# -*- coding: utf-8 -*-
# Copyright (c) 2017, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class AssetMaintenanceTask(Document):
	def autoname(self):
		task_abbr = frappe.get_value("Naming Convention Settings", "Naming Convention", "asset_maintenance_task_abbr")
		itm_cat_abbr = frappe.get_value("Asset Item Naming", {"parent": "Naming Convention", "asset_category": self.asset_category}, "abbr")
		self.naming_series = task_abbr+"-"+itm_cat_abbr+"-.######."
